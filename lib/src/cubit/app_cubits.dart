import 'package:bloc/bloc.dart';
import 'package:project_sqllite/src/cubit/app_cubit_states.dart';
import 'package:project_sqllite/src/models/data_model.dart';
import 'package:project_sqllite/src/services/data_sarvices.dart';

class AppCubits extends Cubit<CubitStates>{
  AppCubits({required this.data}): super(InitiaState()){
    emit(WelcomeState());
}
    final DataServices data;
    late final places;
    void getData()async{
    try{
      emit(LoadingState());
      places = await data.getInfo();
      emit(LoadedState(places));
    }catch(e){

    }

    }

    detailPage(DataModel data){
      emit(DetailState(data));
    }
}